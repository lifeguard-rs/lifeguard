use std::path::PathBuf;

use clap::Parser;
use color_eyre::eyre::{self, anyhow};
use log::warn;

use lifeguard::config::{read_config_or_default, Host, Repository};

#[derive(Parser)]
struct Args {
    #[clap(long, default_value = "lifeguard")]
    prefix: String,
    #[clap(long)]
    dry_run: bool,

    paths: Vec<PathBuf>,
}

fn main() -> eyre::Result<()> {
    env_logger::builder()
        .filter_level(log::LevelFilter::Info)
        .format_timestamp(None)
        .format_module_path(false)
        .format_target(false)
        .init();

    color_eyre::install()?;

    // Load .env
    if let Err(e) = dotenv::dotenv() {
        warn!("Unable to load dotenv: {e:?}");
    }

    let args = Args::parse();

    let mut config = read_config_or_default();

    if args.paths.is_empty() {
        return Err(anyhow!("No paths given"));
    }

    // Insert the Rust repository if it isn't already in the config.
    // NOTE: If we merge configs in load_config: this is that code.
    if config
        .repositories
        .iter()
        .find(|config_repo| config_repo.name == "rust")
        .is_none()
    {
        config.repositories.push(Repository {
            name: "rust".to_string(),
            project: "rust-lang/rust".to_string(),
            host: Host::Github,
        });
    }

    lifeguard::cli::read_and_report(&args.paths, &config, args.dry_run)?;

    Ok(())
}
