use clap::Parser;
use log::warn;

use lifeguard::config::{read_config_or_default, Host, Repository};

// https://github.com/clap-rs/clap/blob/master/examples/cargo-example-derive.rs
#[derive(Parser)]
#[clap(name = "cargo")]
#[clap(bin_name = "cargo")]
enum Cargo {
    Lifeguard(Args),
}

#[derive(clap::Args)]
struct Args {
    #[clap(long, default_value = "lifeguard")]
    prefix: String,
    #[clap(long)]
    dry_run: bool,
}

fn main() {
    env_logger::builder()
        .filter_level(log::LevelFilter::Info)
        .format_timestamp(None)
        .format_module_path(false)
        .format_target(false)
        .init();

    color_eyre::install().unwrap();

    if let Err(e) = dotenv::dotenv() {
        warn!("Unable to load dotenv: {e:?}");
    }

    let Cargo::Lifeguard(args) = Cargo::parse();

    let mut config = read_config_or_default();

    // Insert the Rust repository if it isn't already in the config.
    if config
        .repositories
        .iter()
        .find(|config_repo| config_repo.name == "rust")
        .is_none()
    {
        config.repositories.push(Repository {
            name: "rust".to_string(),
            project: "rust-lang/rust".to_string(),
            host: Host::Github,
        });
    }


    let cmd = cargo_metadata::MetadataCommand::new();
    let metadata = cmd.exec().unwrap();

    // Targets can only depend on files in the same directory and further down.
    // For each target, take the directory the starting target file is in and
    // look recursively for .rs-files. This will find files that aren't reachable
    // from the current crate (e.g. if src/a.rs exists and src/lib.rs does not
    // contain `mod a`) but solving that case requires resolving modules.
    // FIXME: Instead, look in the target file for module declarations and recurse
    // into those files.

    let start_paths: Vec<_> = metadata
        .workspace_members
        .iter()
        .map(|id| {
            let package = &metadata[id];
            let targets = &package.targets;
            let mut target_paths: Vec<_> = targets
                .iter()
                .map(|target| target.src_path.clone())
                .collect();
            for path in target_paths.iter_mut() {
                let _file = path.pop();
            }

            // Remove duplicates. (For example, multiple binaries in src/bin.)
            target_paths.sort();
            target_paths.dedup();

            target_paths
        })
        .flatten()
        .collect();

    // Look for files matching `*.rs`.

    // Store the walkers in a vec so we can borrow paths from them.
    let walker: Vec<_> = start_paths
        .iter()
        .map(|p| walkdir::WalkDir::new(p))
        .flatten()
        .filter_map(|e| e.ok())
        .collect();

    let files: Vec<_> = walker
        .iter()
        .filter(|e| e.path().extension().and_then(|os| os.to_str()) == Some("rs"))
        .map(|e| e.path())
        .collect();

    lifeguard::cli::read_and_report(files, &config, args.dry_run).unwrap();
}
