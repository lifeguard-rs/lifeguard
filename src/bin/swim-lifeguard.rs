use clap::Parser;
use log::warn;

use lifeguard::config::{read_config_or_default, Host, Repository};

// https://github.com/clap-rs/clap/blob/master/examples/cargo-example-derive.rs
#[derive(Parser)]
#[clap(name = "swim")]
#[clap(bin_name = "swim")]
enum Swim {
    Lifeguard(Args),
}

#[derive(clap::Args)]
struct Args {
    #[clap(long, default_value = "lifeguard")]
    prefix: String,
    #[clap(long)]
    dry_run: bool,
}

fn main() {
    env_logger::builder()
        .filter_level(log::LevelFilter::Info)
        .format_timestamp(None)
        .format_module_path(false)
        .format_target(false)
        .init();

    color_eyre::install().unwrap();

    if let Err(e) = dotenv::dotenv() {
        warn!("Unable to load dotenv: {e:?}");
    }

    let Swim::Lifeguard(args) = Swim::parse();

    let swim_config_str = std::fs::read_to_string("swim.toml").unwrap();
    let swim_config =
        toml::from_str::<swim::config::Config>(&swim_config_str).unwrap();

    let mut config = read_config_or_default();

    // Insert the Spade repository if it isn't already in the config.
    if config
        .repositories
        .iter()
        .find(|config_repo| config_repo.name == "spade")
        .is_none()
    {
        config.repositories.push(Repository {
            name: "spade".to_string(),
            project: "spade-lang/spade".to_string(),
            host: Host::Gitlab,
        });
    }

    // Look for files matching `*.spade`.

    // Store the walkers in a vec so we can borrow paths from them.
    let walker: Vec<_> = swim_config.source_dirs
        .iter()
        .map(|p| walkdir::WalkDir::new(p))
        .flatten()
        .filter_map(|e| e.ok())
        .collect();

    let files: Vec<_> = walker
        .iter()
        .filter(|e| e.path().extension().and_then(|os| os.to_str()) == Some("rs"))
        .map(|e| e.path())
        .collect();

    lifeguard::cli::read_and_report(files, &config, args.dry_run).unwrap();
}
