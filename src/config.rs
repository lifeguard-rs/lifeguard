use std::fs;
use std::path::PathBuf;

use log::error;
use log::info;
use serde::Deserialize;

#[derive(Debug, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum Host {
    Github,
    Gitlab,
}

#[derive(Debug, Deserialize)]
pub struct Repository {
    pub name: String,
    pub project: String,
    pub host: Host,
}

#[derive(Debug, Deserialize)]
#[serde(default, deny_unknown_fields)]
pub struct Config {
    pub repositories: Vec<Repository>,
    pub comment_prefix: String,
    pub prefix: String,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            repositories: Default::default(),
            comment_prefix: "//".to_string(),
            prefix: "".to_string(),
        }
    }
}

pub fn read_config_or_default() -> Config {
    // FIXME: Look at parent directories and read configurations from there too.
    //        Open question: should we merge all found files or only look for the first?
    if !PathBuf::from("lifeguard.toml").exists() {
        return Config::default();
    }

    // lifeguard rust#87335: This can be rewritten using let-else.
    let s = match fs::read_to_string("lifeguard.toml") {
        Ok(s) => s,
        Err(e) => {
            error!("Unable to read configuration file: {e}");
            info!("Using default config due to error");
            return Config::default();
        }
    };
    toml::from_str(&s).unwrap_or_else(|e| {
        error!("Invalid configuration file: {e}");
        info!("Using default config due to error");
        Config::default()
    })
}
