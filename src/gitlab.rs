use gitlab::api::Query;
use std::collections::HashMap;
use std::path::PathBuf;

use crate::{Comment, Line, ParseError, ThingStatus};

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Event {
    IssueClosed(u64),
    MergeRequestClosed(u64),
}

impl Event {
    pub fn parse(s: &str) -> Option<Self> {
        s.chars().next().and_then(|c| match c {
            '#' => Some(Event::IssueClosed(
                s.split_once('#')
                    .unwrap()
                    .1
                    .trim_end_matches(":")
                    .parse()
                    .ok()?,
            )),
            '!' => Some(Event::MergeRequestClosed(
                s.split_once('!')
                    .unwrap()
                    .1
                    .trim_end_matches(":")
                    .parse()
                    .ok()?,
            )),
            _ => None,
        })
    }
}

impl std::fmt::Display for Event {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Event::IssueClosed(id) => write!(f, "#{id}"),
            Event::MergeRequestClosed(id) => write!(f, "!{id}"),
        }
    }
}

fn status_of_issues(project: &str, events: Vec<Event>) -> Vec<(Event, ThingStatus)> {
    let token = match std::env::var("LIFEGUARD_GITLAB_TOKEN") {
        Ok(token) => token,
        Err(e) => panic!("specify a gitlab personal access token via env-variable LIFEGUARD_GITLAB_TOKEN (error: {})", e),
    };

    let client = gitlab::Gitlab::new("gitlab.com", token).unwrap();
    let endpoint = gitlab::api::issues::ProjectIssues::builder()
        .project(project)
        .build()
        .unwrap();
    let issues: Vec<gitlab::Issue> = gitlab::api::paged(endpoint, gitlab::api::Pagination::All)
        .query(&client)
        .unwrap();
    let issues: HashMap<Event, ThingStatus> = issues
        .into_iter()
        .map(|issue| {
            (
                Event::IssueClosed(issue.iid.value()),
                match issue.state {
                    gitlab::types::IssueState::Opened => ThingStatus::Open,
                    gitlab::types::IssueState::Reopened => ThingStatus::Open,
                    gitlab::types::IssueState::Closed => ThingStatus::Closed,
                },
            )
        })
        .collect();

    events
        .iter()
        .map(|event| {
            let state = issues.get(event).copied().unwrap_or(ThingStatus::Invalid);
            (*event, state)
        })
        .collect()
}

pub fn filter_events((owner, name): &(String, String), events: Vec<Event>) -> Vec<Event> {
    let project = format!("{}/{}", owner, name);
    status_of_issues(&project, events)
        .into_iter()
        .filter_map(|(event, status)| matches!(status, ThingStatus::Closed).then(|| event))
        .collect()
}

pub fn parse_url_comment<'u>(
    url: &'u url::Url,
    file: PathBuf,
    line: Line,
    message: String,
) -> Result<Comment<Event>, ParseError> {
    // FIXME The below lifetime can't be elided for some reason.
    let parse_url = |url: &'u url::Url| {
        let mut segments = url.path_segments()?;
        let owner = segments.next()?;
        let name = segments.next()?;
        let _ = segments.next()?; // Literal `-`
        let event_kind = segments.next()?;
        let event_num = segments.next()?;
        Some((owner, name, event_kind, event_num))
    };

    let (owner, name, event_kind, event_num) =
        parse_url(url).ok_or(ParseError::UnknownUrl(url.clone()))?;

    let event = match event_kind {
        "issues" => Event::IssueClosed(event_num.parse()?),
        "merge_requests" => Event::IssueClosed(event_num.parse()?),
        _ => return Err(ParseError::UnknownUrlPart(event_kind.to_string())),
    };

    Ok(Comment {
        file,
        line,
        project: (owner.to_string(), name.to_string()),
        event,
        message: message.to_string(),
    })
}
