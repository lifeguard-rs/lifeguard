# Lifeguard

Never miss a `// TODO: .finish_non_exhaustive() when debug_non_exhaustive is
stabilized` again.

Lifeguard looks for comments starting with a URL or alias (see Configuration) to
an issue, pull request or merge request. For any closed ones, it prints the
location of the comment and an optional message.

## Basic usage

1. Write a comment like either `<url>[: <message>]` or `<alias><id>[: <message>]`.
1. Run `lifeguard` on your source files to check if any watched issues have been
   closed. For example: `lifeguard **/*.spade`.

### Example

With the following source file:

```
// spade#109: register can be made expression
reg (clk) x = x;
x
// spade#110
// spade#111
```

When lifeguard is run, it will check if the watched issues and merge requests
have been closed and if so, output a message like this: (here, #109 and #110
have been closed while #111 is still open)

```
[your_file:1] spade-lang/spade#109: register can be made expression
[your_file:4] spade-lang/spade#110
```

## Helper binaries for cargo and swim

We include `cargo-lifeguard` and `swim-lifeguard` for use with `cargo` and
`swim` respectively. To use them, run `cargo lifeguard` or `swim lifeguard`
which will check all respective Rust or Spade source files in the current
project so you don't have to specify the paths yourself. Configuration is still
supported with a `lifeguard.toml` and CLI-flags. See the respective `--help` for more
information.

## Configuration

Lifeguard supports configuration with a `lifeguard.toml` as well as using
command line flags. Run `lifeguard --help` to see the available flags.

### `lifeguard.toml`

Change the comment prefix:

```toml
comment_prefix = "//"
```

Add a prefix that needs to be at the beginning of a comment for lifeguard to
consider it. For example, with

```toml
prefix = "lifeguard"
```

the comment `// lifeguard spade#111` will be read but `// spade#111` will be
ignored.

Add a new repository alias:

```toml
[[repositories]]
name = "spade"
project = "spade-lang/spade"
host = "gitlab"
```

Lifeguard has a built-in alias for [Rust](https://github.com/rust-lang/rust) so
you can try it out in your own code without having to create a configuration
file.

Note that currently, access tokens are expected as the environment variables
`LIFEGUARD_GITHUB_TOKEN` and `LIFEGUARD_GITLAB_TOKEN`. We're using
[`dotenv`](https://github.com/dotenv-rs/dotenv) so the easiest way of supplying
these environment variables is by placing them in a file named `.env`. The
`.env` needs to be located in the current directory or any of its parents to be
found. Your home directory (`/home/your-username` on Linux) is a good default.
**Remember to keep it out of any dotfile repositories!**
